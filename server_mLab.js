require('dotenv').config();
const express = require('express'); //import
const body_parser=require('body-parser');
const request_json=require('request-json');
const app=express();
const port=process.env.PORT || 3000;
const URL_BASE = '/techu/v2/';
const URL_mLab='https://api.mlab.com/api/1/databases/techu8db/collections/';
const apiKey_mlab='apiKey='+process.env.apiKey;
const httpClient = request_json.createClient(URL_mLab);

app.listen(port, function(){
  console.log('Node JS escuchando en el puerto: '+port);
});

app.use(body_parser.json());
app.get(URL_BASE + 'users',
  function(req, res) {
    console.log("Cliente HTTP mLab creado.");
    const fieldParam = 'f={"_id":0}&';
    httpClient.get('user?' + fieldParam+ apiKey_mlab,
      function(err, respuestaMLab, body) {
        console.log('Error: ' + err);
        console.log('Respuesta MLab: ' + respuestaMLab);
        console.log('Body: ' + body);
        var response = {};
        //console.log('Respuesta MLab: ' + JSON.stringify(respuestaMLab));
        if(err) {
            response = {"msg" : "Error al recuperar users de mLab."}
            res.status(500);
        } else {
          if(body.length > 0) {
            response = body;
          } else {
            response = {"msg" : "Usuario no encontrado."};
            res.status(404);
          }
        }
        res.send(response);
      }); //httpClient.get(
});

//peticion GET a un unico usuario mediante ID (instancia)
app.get(URL_BASE + 'users/:id',
  function (req, res) {
    console.log("GET /users/:id");
    console.log(req.params.id);
    var id = req.params.id;
    var queryString = `q={"id_user":${id}}&`;
    var queryStrField = 'f={"_id":0}&';
    httpClient.get('user?' + queryString + queryStrField + apiKey_mlab,
      function(err, respuestaMLab, body){
        console.log("Respuesta mLab correcta.");
      //  var respuesta = body[0];
        var response = {};
        if(err) {
            response = {"msg" : "Error obteniendo usuario."}
            res.status(500);
        } else {
          if(body.length > 0) {
            response = body;
          } else {
            response = {"msg" : "Usuario no encontrado."}
            res.status(404);
          }
        }
        res.send(response);
      });
});

//peticion GET a un unico usuario mediante ID (instancia)
app.get(URL_BASE + 'users/:id/accounts',
  function (req, res) {
    console.log(req.params.id);
    var id = req.params.id;
    var queryString = `q={"id_user":${id}}&`;
    var queryStrField = 'f={"_id":0}&';
    httpClient.get('user?' + queryString + queryStrField + apiKey_mlab,
      function(err, respuestaMLab, body){
        console.log("Respuesta mLab correcta.");
      //  var respuesta = body[0];
        var response = {};
        if(err) {
            response = {"msg" : "Error obteniendo usuario."}
            res.status(500);
        } else {
          if(body.length > 0) {
            response = body[0].account;
          } else {
            response = {"msg" : "Usuario no encontrado."}
            res.status(404);
          }
        }
        res.send(response);
      });
});

//POST of user
app.post(URL_BASE + "users",
 function(req, res) {
  httpClient.get('user?'+ apiKey_mlab ,
  function(error, respuestaMLab , body) {
      newID=body.length+1;
      console.log("newID:" + newID);
      var newUser = {
        "id_user" : newID+1,
        "first_name" : req.body.first_name,
        "last_name" : req.body.last_name,
        "email" : req.body.email,
        "password" : req.body.password
      };
      httpClient.post(URL_mLab + "user?" + apiKey_mlab, newUser ,
       function(error, respuestaMLab, body) {
        res.send(body);
     });
  });
});

//PUT of user
app.put(URL_BASE + 'users/:id',
function(req, res) {
 httpClient.get('user?'+ apiKey_mlab,
 function(error, respuestaMLab , body) {
   console.log("newID:" + newID);
     newID=body.length+1;
     var cambio = '{"$set":' + JSON.stringify(req.body) + '}';
     httpClient.put(URL_mLab + 'user?q={"id_user": ' + newID + '}&' + apiKey_mlab, JSON.parse(cambio),
      function(error, respuestaMLab, body) {
        console.log("body:"+ body);
       res.send(body);
     });
 });
});

//DELETE user with id
app.delete(URL_BASE + "users/:id",
  function(req, res){
    console.log("entra al DELETE");
    console.log("request.params.id: "+req.params.id);
    var id=req.params.id;
    var queryStringID=`q={"id_user":${id}}&`;
    console.log(URL_mLab + 'user?' + queryStringID + apiKey_mlab);
    httpClient.get('user?' +  queryStringID + apiKey_mlab,
      function(error, respuestaMLab, body){
        var respuesta = body[0];
        console.log("body delete:"+ JSON.stringify(respuesta));
        httpClient.delete(URL_mLab + "user/"+ respuesta._id.$oid +'?'+ apiKey_mlab,
          function(error, respuestaMLab,body){
            res.send(body);
        });
      });
  });

//Method POST login
app.post(URL_BASE + "login",
  function (req, res){
    console.log("/login");
    let email = req.body.email;
    let password = req.body.password;
    let queryString = 'q={"email":"' + email + '","password":"' + password + '"}&';
    let limFilter = 'l=1&';
    httpClient.get('user?'+ queryString + limFilter + apiKey_mlab,
      function(error, respuestaMLab, body) {
        if(!error) {
          if (body.length == 1) { // Existe un usuario que cumple 'queryString'
            let login = '{"$set":{"logged":true}}';
            httpClient.put('user?q={"id_user": ' + body[0].id_user + '}&' + apiKey_mlab, JSON.parse(login),
            //clienteMlab.put('user/' + body[0]._id.$oid + '?' + apikeyMLab, JSON.parse(login),
              function(errPut, resPut, bodyPut) {
                res.send({'msg':'Login correcto', 'user':body[0].email, 'id_user':body[0].id_user, 'name':body[0].first_name});
                // If bodyPut.n == 1, put de mLab correcto
              });
          }
          else {
            res.status(404).send({"msg":"Usuario no válido."});
          }
        } else {
          res.status(500).send({"msg": "Error en petición a mLab."});
        }
    });
});

//Method POST logout
app.post(URL_BASE + "logout",
  function(req, res) {
    console.log("/logout");
    var email = req.body.email;
    var queryString = 'q={"email":"' + email + '","logged":true}&';
    //var queryString = `q={"email":${email},"logged":true}&`;
    console.log(queryString);
    httpClient.get('user?'+ queryString + apiKey_mlab,
      function(error, respuestaMLab, body) {
        var respuesta = body[0]; // Asegurar único usuario
        if(!error) {
          if (respuesta != undefined) { // Existe un usuario que cumple 'queryString'
            let logout = '{"$unset":{"logged":true}}';
            httpClient.put('user?q={"id_user": ' + respuesta.id_user + '}&' + apiKey_mlab, JSON.parse(logout),
            //clienteMlab.put('user/' + respuesta._id.$oid + '?' + apikeyMLab, JSON.parse(logout),
              function(errPut, resPut, bodyPut) {
                res.send({'msg':'Logout correcto', 'user':respuesta.email});
                // If bodyPut.n == 1, put de mLab correcto
              });
            } else {
                res.status(404).send({"msg":"Logout failed!"});
            }
        } else {
          res.status(500).send({"msg": "Error en petición a mLab."});
        }
    });
});
